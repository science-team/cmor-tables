# obs4MIPs-cmor-tables
- Up-to-date obs4MIPs CMOR3 tables (json files)
- Up-to-date obs4MIPs Controlled Vocabulary (CVs; json files) 
- Examples for preparing obs4MIPs data with CMOR3 
